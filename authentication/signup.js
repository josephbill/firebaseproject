  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyAfEGPW9p9RDVz_Hnt9nZxO0goHU0nk220",
    authDomain: "fir-websiteproject.firebaseapp.com",
    databaseURL: "https://fir-websiteproject-default-rtdb.firebaseio.com",
    projectId: "fir-websiteproject",
    storageBucket: "fir-websiteproject.appspot.com",
    messagingSenderId: "658574233657",
    appId: "1:658574233657:web:749a7f41d9216295b3fde8",
    measurementId: "G-H3SHX823Z8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


  //listen to submit event 
  document.getElementById("signupForm").addEventListener('submit',function(e){
     e.preventDefault();

     //pick the users input
     var email = document.getElementById("email").value;
     var password = document.getElementById("password").value;
     var username = document.getElementById("username").value;
     var phone = document.getElementById("phone").value;
     var gender = document.getElementById("gender").value;

     //firebase to create account based on email / password 
     //createUserWithEmailAndPassword
     firebase.auth().createUserWithEmailAndPassword(email,password).then(
              function(response){
                console.log(response);
                alert("account created ");
             sendVerificationEmail();

              }
      ).catch(
               function(error){
                 alert("something went wrong " + error);
                 console.log(error);
                 location.reload();
               }
      );

      //take user details to real time db 
      firebase.database().ref("userDetails/").push({
            Email: email,
            password: password,
            username: username,
            gender: gender,
            phoneNumber: phone


      }).then(
            function(response){
              alert("details submitted ");
                   //transition to login

                window.location.replace("login.html");
            }
 
      ).catch(function(error){
          alert("something went wrong " + error);
      });


  });

  //Function called right after the signUpWithEmailAndPassword to send verification emails
function sendVerificationEmail() {
    //Built in firebase function responsible for sending the verification email
    firebase.auth().currentUser.sendEmailVerification()
    .then(() => {
        console.log('Verification Email Sent Successfully !');
        //redirecting the user to the profile page once everything is done correctly
        // window.location.assign('../profile');
    })
    .catch(error => {
        console.error(error);
    })
}