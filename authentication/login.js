  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyAfEGPW9p9RDVz_Hnt9nZxO0goHU0nk220",
    authDomain: "fir-websiteproject.firebaseapp.com",
    databaseURL: "https://fir-websiteproject-default-rtdb.firebaseio.com",
    projectId: "fir-websiteproject",
    storageBucket: "fir-websiteproject.appspot.com",
    messagingSenderId: "658574233657",
    appId: "1:658574233657:web:749a7f41d9216295b3fde8",
    measurementId: "G-H3SHX823Z8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  //listen for submit click action
  document.getElementById("loginForm").addEventListener('submit', function(e){
              e.preventDefault();

              //capture user input
              var email = document.getElementById("email").value;
              var password = document.getElementById("password").value;

              //firebase to verify and login user 
              //signInWithEmailAndPassword

              firebase.auth().signInWithEmailAndPassword(email,password).then(
                    function(response){
                      alert("account verified ");
                      window.location.replace("../public/recordsDisplay.html");
                    }

                ).catch(function(error){
                      var errorCode = error.code;
                      if (errorCode == "auth/wrong-password") {
                         alert("wrong password entry");
                         location.reload();
                      } else {
                        alert(error);
                      }

                      alert("something went wrong , try again " + error);
                      location.reload();

                })


  });

//reset 

document.getElementById("resetPass").addEventListener('submit', function(e){
  e.preventDefault();

  //capture user reset email 
  var resetEmail = document.getElementById("emailReset").value;

  //firebase code to reset email : sendPasswordResetEmail
  firebase.auth().sendPasswordResetEmail(resetEmail).then(function(response){
       console.log(response);
       alert("password reset link sent to email");
  }).catch(function(error){
       console.log(error);
       alert("reset link not sent " + error);
  });
});


//reset password via firebase 
//listen for form event 
document.getElementById("resetForm").addEventListener('submit', function(e){
     e.preventDefault();

     //capture user email 
     var resetEmail = document.getElementById("resetEmail").value;

     //use firebase method sendPasswordResetEmail to send a reset link
     firebase.auth().sendPasswordResetEmail(resetEmail).then(
      function(response){
          console.log(response);
          alert("password reset link sent");
      }).catch(function(error){
        console.log(error);
          alert("Failed " + error.code); 
      });
});


//google sign in
function googleLogin(){
    //intializing google sign up 
    var base_provider = new firebase.auth.GoogleAuthProvider();

    //signin using the google firebase method signInWithPopUP from firebase 
    firebase.auth().signInWithPopup(base_provider).then(function(response){
             console.log(response);
             alert("sign in success");
             //redirect screen
             window.location.replace("../public/recordsDisplay.html");


    }).catch(function(error){
         console.log(error);
         alert("sign in with google failed " + error.code);
    })
}