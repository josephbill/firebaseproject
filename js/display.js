  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyAfEGPW9p9RDVz_Hnt9nZxO0goHU0nk220",
    authDomain: "fir-websiteproject.firebaseapp.com",
    databaseURL: "https://fir-websiteproject-default-rtdb.firebaseio.com",
    projectId: "fir-websiteproject",
    storageBucket: "fir-websiteproject.appspot.com",
    messagingSenderId: "658574233657",
    appId: "1:658574233657:web:749a7f41d9216295b3fde8",
    measurementId: "G-H3SHX823Z8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);


   //create ref to table interface 
   var tableRecords = document.getElementById("tableRecords");
   //ref to the firebase database 
   var databaseRecords = firebase.database().ref('studentDetails/');
   //default row for my table 
   var rowIndex = 1;

   //picking values in the firebase ref node
   databaseRecords.once('value', function(snapshotRecords){
        //forEach to loop through records
        snapshotRecords.forEach(function(childSnapShotRecords){
            //key record
            var childKey = childSnapShotRecords.key;
            //data values
            var childData = childSnapShotRecords.val();
            //define row iteration
            var row = tableRecords.insertRow(rowIndex);
            //get image path
            var image = childSnapShotRecords.val().studentImage;

            //defining the cell structure
            var cellId = row.insertCell(0);
            var cellName = row.insertCell(1);
            var cellEmail = row.insertCell(2);
            var cellAge = row.insertCell(3);
            var cellImage = row.insertCell(4).outerHTML = "<tr id='" + rowIndex + "'><td><a href=" + image + " target='_blank'><img style='width: 140px; height: 140px;' src=" + image + "></img></a></td></tr>"

           

           //map the txt content to cells
           cellId.appendChild(document.createTextNode(childKey));
           cellName.appendChild(document.createTextNode(childData.studentName));
           cellEmail.appendChild(document.createTextNode(childData.studentEmail));
           cellAge.appendChild(document.createTextNode(childData.studentAge));

        })



   })


   //delete 
   function deleteRecordbyId(){
       //pick the id of the record being deleted 
       var deleteId = document.getElementById("deleteRecordId").value;
       // remove function to delete data in fb realtime db
       firebase.database().ref('studentDetails/').child(deleteId).remove();
       //alert 
       alert("record deleted");
       deleteId ='';
       //reload 
       location.reload();
   }

   //update n
   function updateRecordbyId(){
      //pick id of the record 
      var updateid = document.getElementById("updateRecordId").value;
      // picking new email
      var newEmail = document.getElementById("updateEmail").value;
      var newName = document.getElementById("updateName").value;
      //update function to update data in record node in FB 
      firebase.database().ref('studentDetails/' + updateid).update({
          studentEmail: newEmail,
          studentName: newName

      })

      alert("update successful");
      newEmail = '';
      updateid = '';
      location.reload();
   }


   //signout 
   function signOut(){
        //signOut
        firebase.auth().signOut().then(
             function(response){
                alert("logged out");
                window.location.replace("../authentication/login.html")
             }
          ).catch(function(error){
              alert(error);
              location.reload();
          })

       }