  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyAfEGPW9p9RDVz_Hnt9nZxO0goHU0nk220",
    authDomain: "fir-websiteproject.firebaseapp.com",
    databaseURL: "https://fir-websiteproject-default-rtdb.firebaseio.com",
    projectId: "fir-websiteproject",
    storageBucket: "fir-websiteproject.appspot.com",
    messagingSenderId: "658574233657",
    appId: "1:658574233657:web:749a7f41d9216295b3fde8",
    measurementId: "G-H3SHX823Z8"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);



  //listen for the submit button in form 
  document.getElementById("formUpload").addEventListener('submit', function(e){
          //prevent default entries 
          e.preventDefault();


          //capture users data 
          //variables to store user input 
          var username = document.getElementById("name").value;
          var age = document.getElementById("age").value;
          var email = document.getElementById("email").value;

          //console testing 
          console.log(username + age + email);


          //setting up storage 
          //reference to the storage
          const ref = firebase.storage().ref('studentImages/');
          //pick our image 
          const file = document.querySelector("#imagePick").files[0];
          //picking a name for image
          const name = +new Date() + "-" + file.name;
          //domain, type of image
          const metadata = {
            contentType: file.type
          };
          //pushing upload to storage bucket 
          const task = ref.child(name).put(file, metadata);
          //setting promises for what happens next
          task 
            .then(snapshot => snapshot.ref.getDownloadURL())
            //want to send data to real time database 
            .then(url => {
               console.log(url);
               

              //reference to the realtime database 
               firebase.database().ref('studentDetails/').push({
                     studentName: username,
                     studentEmail: email,
                     studentAge: age,
                     studentImage: url


               })

               alert("student added");
               username = "";
               email = "";
               age = "";


            })
             .catch(console.error);


  });
 